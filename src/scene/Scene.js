import * as PIXI from 'pixi.js';
import _ from 'underscore';

import TweenMax from "gsap/TweenMax";

import {Creature, CreatureEvent} from "./Creature";

import Utils from "../utils/Utils";
import Sounds from "../utils/Sounds";

export default class Scene extends PIXI.Container {
    constructor() {
        super();

        this.creatureCollection = [];
        this.creatureContainer = new PIXI.Container();
        this.addChild(this.creatureContainer);

        this.creatureSlots = [
            {x: -300, y: -150, free: true, creature: null},
            {x: 0, y: -150, free: true, creature: null},
            {x: 300, y: -150, free: true, creature: null},
            {x: -300, y: 150, free: true, creature: null},
            {x: 0, y: 150, free: true, creature: null},
            {x: 300, y: 150, free: true, creature: null}
        ];

        this.creatureMaxCount = 3;
        this.creatureSpawnDelay = 2;
        this.creatureCompleteId = 5;

        this.startCreatureSpawnTimer();
    }

    update(dt) {
    }

    startCreatureSpawnTimer() {
        this.creatureSpawnTimerId = setInterval(() => {
            if (this.creatureCollection.filter(o => o.id === 1).length < this.creatureMaxCount) {
                this.addCreature(1, this.getFreeSlot());
            }
        }, this.creatureSpawnDelay * 1000)
    }

    stopCreatureSpawnTimer() {
        if (this.creatureSpawnTimerId > -1) {
            clearInterval(this.creatureSpawnTimerId);
            this.creatureSpawnTimerId = -1;
        }
    }

    addCreature(id, slot) {
        if (typeof slot === 'undefined') {
            slot = this.getFreeSlot();
        }

        const creature = new Creature(id);

        slot.free = false;
        slot.creature = creature;

        creature.position.set(slot.x, slot.y);
        creature.show();
        creature.on(CreatureEvent.StartDragging, this.onCreatureStartDragging.bind(this, creature));
        creature.on(CreatureEvent.Dragging, this.onCreatureDragging.bind(this, creature));
        creature.on(CreatureEvent.StopDragging, this.onCreatureStopDragging.bind(this, creature));

        this.creatureContainer.addChild(creature);
        this.creatureCollection.push(creature);

        return creature;
    }

    removeCreature(creature) {
        const index = this.creatureCollection.indexOf(creature);

        if (index > -1) {
            const slot = this.creatureSlots.find(o => o.creature === creature);

            slot.free = true;
            slot.creature = null;

            this.creatureCollection.splice(index, 1);
            this.creatureContainer.removeChild(creature);
        }
    }

    getFreeSlot() {
        return _.sample(this.creatureSlots.filter(o => o.free));
    }

    onCreatureStartDragging(creature) {
        this.creatureContainer.setChildIndex(creature, this.creatureContainer.children.length - 1);
    }

    onCreatureDragging(creature) {
        this.creatureContainer.setChildIndex(creature, this.creatureContainer.children.length - 1);
    }

    onCreatureStopDragging(creature) {
        const creatures = this.creatureCollection.filter(o => o !== creature);

        for (let i = 0; i < creatures.length; i++) {
            const neighbor = creatures[i];

            if (creature.id === neighbor.id) {
                const creatureBounds = creature.getBounds();
                const neighborBounds = neighbor.getBounds();

                const spwanSlot = this.creatureSlots.find(o => o.creature === neighbor);

                if (Utils.hitTest(creatureBounds, neighborBounds)) {
                    const nextId = creature.id + 1;

                    if (nextId === this.creatureCompleteId) {
                        this.stopCreatureSpawnTimer();

                        for (let i = 0; i < this.creatureCollection.length; i++) {
                            const otherCreature = this.creatureCollection[i];

                            if (otherCreature !== creature) {
                                otherCreature.hide().then(() => {
                                    this.removeCreature.call(this, otherCreature);
                                });
                            }
                        }

                        creature.hide().then(() => {
                            this.removeCreature.call(this, creature);

                            const newCreature = this.addCreature(nextId, spwanSlot);

                            newCreature.interactive = false;
                            newCreature.buttonMode = false;

                            TweenMax.to(newCreature, 0.3, {
                                x: 0, y: 0, onComplete: () => {
                                    const simpleAnim = () => {
                                        TweenMax.to(newCreature.scale, 0.4, {
                                            x: 1.2, y: 1.2, onComplete: () => {
                                                TweenMax.to(newCreature.scale, 0.4, {
                                                    x: 1, y: 1, onComplete: () => {
                                                        simpleAnim();
                                                    }
                                                });
                                            }
                                        });
                                    };

                                    simpleAnim();
                                }
                            });
                        });

                    } else {
                        neighbor.hide().then(() => {
                            this.removeCreature.call(this, neighbor);
                        });
                        creature.hide().then(() => {
                            this.removeCreature.call(this, creature);
                            this.addCreature(nextId, spwanSlot);
                        });
                    }

                    Sounds.play('merge_mp3');

                    return;
                }
            }
        }

        const slot = this.creatureSlots.find(o => o.creature === creature);

        creature.buttonMode = false;
        creature.interactive = false;

        TweenMax.to(creature, 0.2, {
            x: slot.x, y: slot.y, onComplete: () => {
                creature.buttonMode = true;
                creature.interactive = true;
            }
        });
    }
}