import * as PIXI from 'pixi.js';

import TweenMax from "gsap/TweenMax";

import Resources from "../Resources";

export const CreatureEvent = {
    StartDragging: 'CreatureEventStartDragging',
    StopDragging: 'CreatureEventStopDragging',
    Dragging: 'CreatureEventDragging',
};

export class Creature extends PIXI.Container {
    constructor(id) {
        super();

        this.id = id;
        this.name = 'creature' + this.id;

        this.alpha = 0;
        this.interactive = true;
        this.buttonMode = true;

        this.dragging = false;
        this.draggingData = null;

        this.images = [
            'creature1_png',
            'creature2_png',
            'creature3_png',
            'creature4_png',
            'creature5_png'
        ];

        this.sprite = new PIXI.Sprite(Resources.get(this.images[this.id - 1]));
        this.sprite.anchor.set(0.5);
        this.addChild(this.sprite);

        this.on('pointerdown', this.onPointerDown);
        this.on('pointermove', this.onPointerMove);
        this.on('pointerup', this.onPointerUp);
    }

    show () {
        return new Promise(resolve => {
            TweenMax.to(this, 0.1, {
                alpha: 1, onComplete: () => {
                    resolve();
                }
            });
        });
    }

    hide () {
        return new Promise(resolve => {
            TweenMax.to(this, 0.1, {
                alpha: 0, onComplete: () => {
                    resolve();
                }
            });
        });
    }

    onPointerDown(e) {
        this.draggingData = e.data;
        this.dragging = true;

        this.emit(CreatureEvent.StartDragging);
    }

    onPointerUp(e) {
        this.draggingData = null;
        this.dragging = false;

        this.emit(CreatureEvent.StopDragging);
    }

    onPointerMove() {
        if (this.dragging) {
            const newPosition = this.draggingData.getLocalPosition(this.parent);
            const globalPosition = this.parent.toGlobal(newPosition);

            if (globalPosition.x > this.getBounds().width / 2 && globalPosition.x < app.currWidth - this.getBounds().width / 2) {
                this.position.x = newPosition.x;
            }
            if (globalPosition.y > this.getBounds().height / 2 && globalPosition.y < app.currHeight - this.getBounds().height / 2) {
                this.position.y = newPosition.y;
            }

            this.emit(CreatureEvent.Dragging);
        }
    }
}