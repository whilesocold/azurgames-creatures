import * as PIXI from 'pixi.js';

import config from './config';

import Scene from "./scene/Scene";
import Resources from "./Resources";
import Sounds from "./utils/Sounds";

export default class App {
    constructor() {
        window.config = config;

        this.onMRAIDLoadedBind = this.onMRAIDLoaded.bind(this);

        if (typeof mraid !== 'undefined') {
            if (mraid.getState() === 'loading') {
                mraid.addEventListener('ready', this.onMRAIDLoadedBind);

            } else {
                this.init();
            }

        } else {
            this.init();
        }
    }

    onMRAIDLoaded() {
        mraid.removeEventListener('ready', this.onMRAIDLoadedBind);

        this.init();
    }

    init() {
        this.initAssets()
            .then(() => {
                this.initSounds();
                this.initCanvas();
                this.initPIXI();
                this.initBackground();
                this.initScene();
                this.initTicker();

                Sounds.play('music_mp3', { loop: true });

                this.resize();
            });
    }

    initAssets() {
       return new Promise((resolve, reject) => {
           Resources.loadAll().then(() => resolve());
       });
    }

    initSounds() {
        Sounds.init(Resources.soundMap);
    }

    initCanvas() {
        this.width = 800;
        this.height = 600;

        this.currWidth = this.width;
        this.currHeight = this.height;

        this.canvas = document.getElementById('canvas');
        this.canvas.width = this.width;
        this.canvas.height = this.height;
    }

    initPIXI() {
        this.app = new PIXI.Application({
            view: this.canvas,
            transparent: true
        });

        this.renderer = this.app.renderer;
        this.stage = this.app.stage;

        this.container = new PIXI.Container();
        this.stage.addChild(this.container);
    }

    initBackground() {
        this.background = new PIXI.Sprite(Resources.get('background_jpg'));
        this.background.anchor.set(0.5);
        this.container.addChild(this.background);
    }

    initScene() {
        this.scene = new Scene();
        this.container.addChild(this.scene);
    }

    initTicker() {
        this.app.start();
        this.app.ticker.add(this.update, this);
    }

    update(dt) {
        if (window.innerWidth !== this.currWidth || window.innerHeight !== this.currHeight) {
            this.resize();
        }

        if (this.scene) {
            this.scene.update(dt);
        }
    }

    resize(width, height) {
        let currWidth = window.innerWidth;
        let currHeight = window.innerHeight;

        if (typeof mraid !== 'undefined') {
            const  size = mraid.getScreenSize();

            currWidth = size.width;
            currHeight = size.height;
        }

        const backgroundScale = Math.max(currWidth / this.background.texture.baseTexture.width, currHeight / this.background.texture.baseTexture.height);
        const sceneScale = Math.min(currWidth / this.width, currHeight / this.height);

        this.currWidth = currWidth;
        this.currHeight = currHeight;

        this.canvas.width = currWidth;
        this.canvas.height = currHeight;

        this.renderer.resize(currWidth, currHeight);

        this.background.position.set(currWidth / 2, currHeight / 2);
        this.background.scale.set(backgroundScale);

        this.scene.position.set(currWidth / 2, currHeight / 2);
        this.scene.scale.set(sceneScale);
    }
}