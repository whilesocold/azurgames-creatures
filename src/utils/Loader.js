import * as PIXI from 'pixi.js';

class Loader {
    static keyToUrlList = [];

    static loadFromMap(map, onProgress, onComplete) {
        if (Object.keys(map).length > 0) {
            for (const key in map) {
                const url = map[key];

                if (!PIXI.loader.resources[url]) {
                    Loader.keyToUrlList[key] = url;

                    PIXI.loader.add(url);
                }
            }
            PIXI.loader.load(onComplete);
            PIXI.loader.on('progress', () => {
                if (onProgress) {
                    onProgress(parseInt(PIXI.loader.progress));
                }
            });

        } else {
            onComplete();
        }
    }

    static getUrl(key) {
        return Loader.keyToUrlList[key];
    }

    static getData(key) {
        return PIXI.loader.resources[Loader.getUrl(key)].data;
    }

    static getTexture(key) {
        return PIXI.Texture.from(PIXI.loader.resources[Loader.getUrl(key)].data);
    }
}

export default Loader;
