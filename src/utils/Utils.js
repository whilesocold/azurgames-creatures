export default class Utils {
    static hitTest(a, b) {
        if (b.x < a.x + a.width && a.x < b.x + b.width && b.y < a.y + a.height) {
            return a.y < b.y + b.height;
        }
        return false;
    }
}