require('dotenv').config('.env');

const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

const DEV_BUILD = process.argv.indexOf('-p') === -1;
const GADS_BUILD = process.env.NODE_ENV === 'gads';
const APPLOVIN_BUILD = process.env.NODE_ENV === 'applovin';

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ProvidePlugin = require('webpack-provide-global-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const OptimizeJsPlugin = require("optimize-js-plugin");

console.log(process.env.NODE_ENV, GADS_BUILD, APPLOVIN_BUILD);

module.exports = {
    entry: {
        bundle: './src/index.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: DEV_BUILD ? 'bundle.js' : 'bundle.[hash].js'
    },
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: {
                DEV_BUILD: DEV_BUILD,
                GADS_BUILD: GADS_BUILD,
                APPLOVIN_BUILD: APPLOVIN_BUILD
            }
        }),
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: DEV_BUILD ? './static/index_dev.html' : (GADS_BUILD ? './static/index_gads.html' : './static/index_applovin.html'),
            filename: 'index.html',
            publicPath: '/',
            inject: 'head',
            WEBSOCKET: process.env.WEBSOCKET,
            inlineSource: '.(js|css)$'
        }),
        new webpack.optimize.UglifyJsPlugin({
            uglifyOptions: {
                mangle: true,
                output: {
                    comments: false
                }
            }
        }),
        new OptimizeJsPlugin({
            sourceMap: false
        }),
        new HtmlWebpackInlineSourcePlugin(),
        new ScriptExtHtmlWebpackPlugin(),
        /*
        new CopyWebpackPlugin([{
            from: './static',
            to: './static'
        }]),
        */
        new ProvidePlugin({
            '_': 'underscore'
        })
    ],
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'babel-loader?presets[]=es2015&plugins[]=transform-class-properties',
            exclude: /(node_modules|bower_components)/
        }, {
            test: /\.hbs/,
            loader: 'handlebars-loader',
            exclude: /(node_modules|bower_components)/
        }, {
            test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
            use: 'base64-inline-loader?limit=1000&name=[name].[ext]'
        }]
    }
};